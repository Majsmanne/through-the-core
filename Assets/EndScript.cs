﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScript : MonoBehaviour
{
    public Camera MainCamera;
    public Camera EndCamera;
    public GameObject Foreground;
    public GameObject EndText;

    public void End()
    {
        Foreground.SetActive(true);
        MainCamera.enabled = false;
        EndCamera.enabled = true;
        var move = GetComponent<CutsceneMove>();
        move.OnFinish = () =>
        {
            EndText.SetActive(true);
        };

        move.enabled = true;
    }
}

