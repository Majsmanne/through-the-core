﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroScript : MonoBehaviour
{
    public Camera Camera1;
    public Camera Camera2;
    public Camera Camera3;
    public Camera Camera4;
    public Camera Camera5;

    public float switch1;
    public float switch2;
    public float switch3;
    public float switch4;

    public float _gameStart = 20f;

    public ScaleInComponent Asteroid;
    public CutsceneMove Ship1;
    public CutsceneMove Ship2;
    private float _sceneStart;
    private void Start()
    {
        _sceneStart = Time.time;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (Elapsed > _gameStart)
        {
            SceneManager.LoadScene("scene");
            enabled = false;
        }
        else if (Elapsed > switch4)
        {
            Ship2.enabled = true;
            Camera4.gameObject.SetActive(false);
            Camera5.gameObject.SetActive(true);
            Debug.Log("cam 5");
        }
        else if (Elapsed > switch3)
        {
            Ship1.enabled = true;
            Asteroid.enabled = false;
            Camera3.gameObject.SetActive(false);
            Camera4.gameObject.SetActive(true);
            Debug.Log("cam 4");
        }
        else if (Elapsed > switch2)
        {
            Asteroid.enabled = true;
            Camera2.gameObject.SetActive(false);
            Camera3.gameObject.SetActive(true);
            Debug.Log("cam 3");
        }
        else if (Elapsed > switch1)
        {
            Camera1.gameObject.SetActive(false);
            Camera2.gameObject.SetActive(true);
            Debug.Log("cam 2");
        }

    }

    private float Elapsed
    {
        get { return Time.time - _sceneStart; }
    }
}