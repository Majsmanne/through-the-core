﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartComponent : MonoBehaviour
{

    public PartCounter PartCounter;

    private void OnTriggerEnter2D(Collider2D other)
    {
        PartCounter.AddPart();
        gameObject.SetActive(false);
    }
}

