﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartCounter : MonoBehaviour
{
    public Texture PartsImage;

    public int Parts
    {
        get { return _parts; }
    }

    private int _parts = 0;

    private bool _showGui = false;

    public void ShowGui()
    {
        _showGui = true;
    }

    private void OnGUI()
    {
        if (!_showGui) return;

        GUI.DrawTexture(new Rect(0, 0, 100, 100), PartsImage);

        var partsString = _parts + " / 5";
        GUI.Label(new Rect(100, 20, 100, 40), partsString);
    }

    public void AddPart()
    {
        _parts++;
    }
}
