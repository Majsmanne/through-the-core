﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleInComponent : MonoBehaviour
{

    public float ScaleSpeed;

    private float _current;
	// Use this for initialization
	void Start () {
		transform.localScale = Vector3.zero;
	    _current = 0;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = Vector3.one * _current;
	    _current += ScaleSpeed * Time.deltaTime;
	}
}
