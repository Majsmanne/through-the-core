﻿using UnityEngine;

public class BouncerComponent : MonoBehaviour
{

    public BouncerComponent Twin;
    public CircularGravityComponent ObjectOn;

    public bool Cheat = false;
	// Use this for initialization

    private void OnTriggerEnter2D(Collider2D other)
    {
        var grav = other.GetComponent<CircularGravityComponent>();
        var body = other.GetComponent<Rigidbody2D>();
        var player = other.GetComponent<PlayerMovement>();

        if (!grav || !body) return;

        if (ObjectOn != null) return;
        if (!(grav.GravityMultiplier > 5)) return;
        if (!(body.velocity.magnitude * body.mass > 10) && !Cheat) return;

        if (player != null) player.DumbFlag = true;

        ObjectOn = grav;
        Twin.Launch();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var grav = other.GetComponent<CircularGravityComponent>();
        if (ObjectOn == grav) ObjectOn = null;

        var player = other.GetComponent<PlayerMovement>();
        if (player != null) player.DumbFlag = true;
    }

    public void Launch()
    {
        if (!ObjectOn) return;

        var body = ObjectOn.GetComponent<Rigidbody2D>();

        body.AddForce(ObjectOn.transform.position.normalized * 25, ForceMode2D.Impulse);
    }
}
