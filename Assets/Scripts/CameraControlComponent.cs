﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraControlComponent : MonoBehaviour
{
    public Transform Target;
	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void LateUpdate ()
	{
	    transform.position = Target.position;

        if(Vector3.Dot(Target.up, Vector3.down) > 0.999999) transform.localEulerAngles = new Vector3(0, 0, 180);
	    else transform.up = Target.up;
	}
}
