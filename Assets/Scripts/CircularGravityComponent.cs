﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CircularGravityComponent : MonoBehaviour {
    public float Gravity = 10f;
    public float GravityMultiplier = 1f;

    private Rigidbody2D _rigidBody;
    private void Start () {
        _rigidBody = GetComponent<Rigidbody2D>();
        _rigidBody.gravityScale = 0;
	}

    private void FixedUpdate () {
        var forceDirection = -transform.position.normalized;
        _rigidBody.AddForce(forceDirection * Gravity * GravityMultiplier * _rigidBody.mass, ForceMode2D.Force);

        if(Vector3.Dot(forceDirection, Vector3.up) > 0.999999)
        {
            transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else transform.up = -forceDirection;
	}
}
