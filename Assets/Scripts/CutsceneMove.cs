﻿using System;
using UnityEngine;

public class CutsceneMove : MonoBehaviour
{
    public Transform Target;
    public Action OnFinish;

    public float speed;
	// Use this for initialization

	// Update is called once per frame
	void Update ()
	{
	    transform.position = Vector3.MoveTowards(transform.position, Target.position, speed * Time.deltaTime);
	    if ((transform.position - Target.position).magnitude < 0.1)
	    {
	        if (OnFinish != null) OnFinish();
	        enabled = false;
	    }
	}
}
