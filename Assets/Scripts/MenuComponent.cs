﻿using UnityEngine;

public class MenuComponent : MonoBehaviour
{

    private bool _showGui = false;

    private void Start()
    {
        Cursor.visible = false;
    }

    void Update () {
	    if (Input.GetKeyDown(KeyCode.Escape))
	    {
	        ToggleGui();
	    }
	}

    private void OnGUI()
    {
        if(!_showGui) return;

        var sw = Screen.width;
        var sh = Screen.height;

        GUI.Label(new Rect(sw / 2 - 150, 100, 300, 30), "Repair your ship and leave the planet.");
        GUI.Label(new Rect(sw / 2 - 100, 200, 200, 40), "Standard controls:");
        GUI.Label(new Rect(sw / 2 - 100, 240, 200, 40), "A and D to move");
        GUI.Label(new Rect(sw / 2 - 100, 280, 200, 40),
            "Space to jump. Hold down to jump higher and reduce gravity (with powerup.)");
        GUI.Label(new Rect(sw / 2 - 100, 320, 200, 40), "Shift to increase gravity (when you've found the powerup.)");
        if (GUI.Button(new Rect(sw / 2 - 50, sh - 200, 100, 50), "Resume"))
        {
            ToggleGui();
        }
        if (GUI.Button(new Rect(sw / 2 - 50, sh - 100, 100, 50), "Quit"))
        {
            Application.Quit();
        }
    }

    private void ToggleGui()
    {
        _showGui = !_showGui;
        Cursor.visible = _showGui;
        Time.timeScale = _showGui ? 0 : 1;
    }
}
