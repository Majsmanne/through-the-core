﻿using System;
using Boo.Lang;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    public float Acceleration = 20f;
    public float AirAcceleration = 20f;
    public float MaxSpeed = 10f;
    public float JumpImpulse = 10f;
    public float JumpAddForce = 10f;

    public bool GlideActivated;
    public bool GravBoostActivated;

    public List<Sprite> RunAnimation;
    public Transform GroundProbe;
    public LayerMask WorldMask;

    public SpriteRenderer GravEffect;
    public SpriteRenderer GlideEffect;

    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _renderer;

    private Vector2 _inputForces = Vector2.zero;
    private Vector2 _inputImpulses = Vector2.zero;

    public bool DumbFlag;

    private float _lastOnGround;
	void Start ()
	{
	    _rigidbody = GetComponent<Rigidbody2D>();
	    _animator = GetComponent<Animator>();
	    _renderer = GetComponent<SpriteRenderer>();
	}

    private bool jump = false;
    private void Update()
    {
        jump = jump || Input.GetButtonDown("Jump");
    }

    // Update is called once per frame
	private void CalcMovement()
	{
		_inputForces = Vector2.zero;
	    _inputImpulses = Vector2.zero;

	    var up = new Vector2(transform.up.x, transform.up.y);
	    var right = new Vector2(transform.right.x, transform.right.y);

        var horVel = Vector2.Dot(right, _rigidbody.velocity);

	    var horInput = Input.GetAxis("Horizontal");

	    var gliding = Vector2.Dot(up, _rigidbody.velocity) < 0 && Input.GetButton("Jump") && GlideActivated;
	    var gravBoost = Input.GetButton("Grav") && GravBoostActivated;

	    if (gliding)
	    {
	        GlideEffect.gameObject.SetActive(true);
	        GravEffect.gameObject.SetActive(false);
	    } else if (gravBoost)
	    {
	        GlideEffect.gameObject.SetActive(false);
	        GravEffect.gameObject.SetActive(true);
	    } else
	    {
	        GlideEffect.gameObject.SetActive(false);
	        GravEffect.gameObject.SetActive(false);
	    }
	    GetComponent<CircularGravityComponent>().GravityMultiplier = gliding ? 0.2f : gravBoost ? 10f : 1f;
	    if (Time.time - _lastOnGround < 0.2)
	    {
	        if(Mathf.Abs(horVel) <= MaxSpeed) _inputForces += right * horInput * Acceleration;

	        if (jump)
	        {
	            _rigidbody.velocity = right * horVel;
                _inputImpulses += up * JumpImpulse;
	        }

	        if (Mathf.Abs(horVel) > 0.05 && Mathf.Abs(horInput) < 0.05)
	        {
	            if(DumbFlag) _rigidbody.velocity -= right * horVel;
	            else _rigidbody.velocity = Vector2.zero;
	        }
	    }
	    else
	    {
	        if (Input.GetButton("Jump") && Vector2.Dot(up, _rigidbody.velocity) > 0)
	        {
                _inputForces += up * JumpAddForce;
	        }
	        if (!(Math.Abs(Mathf.Sign(horInput) - Mathf.Sign(horVel)) < 0.05) || !(Mathf.Abs(horVel) > MaxSpeed))
	        {
	            _inputForces += right * horInput * AirAcceleration;
	        }
	    }
	    CalcAnimations(horVel);
	    jump = false;
	}

    private void CalcAnimations(float horizontalSpeed)
    {
        if (Mathf.Abs(horizontalSpeed) < 0.15)
        {
            _animator.SetBool("running", false);
        }
        else
        {
            _animator.SetBool("running", true);
            _renderer.flipX = horizontalSpeed < 0;
        }
        _animator.SetBool("inAir", Time.time - _lastOnGround >= 0.2);
    }

    private void FixedUpdate()
    {
        CalcMovement();
        _rigidbody.AddForce(_inputForces * _rigidbody.mass, ForceMode2D.Force);
        _rigidbody.AddForce(_inputImpulses, ForceMode2D.Impulse);
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        foreach (var contact in other.contacts)
        {
            if (Vector2.Dot(contact.normal, transform.up) > 0.5)
            {
                _lastOnGround = Time.time;
            }
        }
    }
}
