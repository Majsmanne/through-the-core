﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupComponent : MonoBehaviour
{
    public bool IsGlide = true;
    public PartCounter PartCounter;
    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerMovement>();
        if (IsGlide) player.GlideActivated = true;
        else player.GravBoostActivated = true;
        PartCounter.AddPart();
        gameObject.SetActive(false);
    }
}
