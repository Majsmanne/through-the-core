﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupEffect : MonoBehaviour
{
    public float Freq = 1;

    private SpriteRenderer _spr;
	// Update is called once per frame
    private void Start()
    {
        _spr = GetComponent<SpriteRenderer>();
    }

    void Update ()
	{
	    var f = Time.time % Freq;
	    _spr.flipX = f < Freq / 2;
	}
}
