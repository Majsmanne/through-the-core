﻿using UnityEngine;

public class RotatorComponent : MonoBehaviour {

	// Use this for initialization
    public float speed = 1f;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));
	}
}
