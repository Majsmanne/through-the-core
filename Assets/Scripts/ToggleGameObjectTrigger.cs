﻿using UnityEngine;

public class ToggleGameObjectTrigger : MonoBehaviour
{
    public GameObject Target;
    public bool Activate;
    public LayerMask PlayerLayer;

    private void OnTriggerEnter2D(Collider2D other)
    {
            Target.SetActive(Activate);
    }
}
