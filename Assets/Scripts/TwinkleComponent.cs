﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwinkleComponent : MonoBehaviour
{
    private float _timeScale;
    private float _scale;

    private void Start()
    {
        _scale = Random.value * 3;
        _timeScale = Random.value * 8;
    }

    void Update ()
	{
	    transform.localScale = Vector3.one + Vector3.one * Mathf.Sin(Time.time * _timeScale) * _scale;
	}
}
