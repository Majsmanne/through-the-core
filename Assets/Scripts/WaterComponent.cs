﻿using UnityEngine;

public class WaterComponent : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D other)
    {
        var gravityComponent = other.GetComponent<CircularGravityComponent>();

        if(gravityComponent == null) return;

        var rigidBody = gravityComponent.GetComponent<Rigidbody2D>();

        rigidBody.velocity = Vector2.zero;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        var gravityComponent = other.GetComponent<CircularGravityComponent>();

        if(gravityComponent == null) return;

        var rigidBody = gravityComponent.GetComponent<Rigidbody2D>();


        rigidBody.AddForce(other.transform.up * 10.5f);
    }
}
