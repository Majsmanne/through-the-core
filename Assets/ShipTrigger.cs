﻿using UnityEngine;

public class ShipTrigger : MonoBehaviour
{
    public PartCounter PartCounter;
    public EndScript End;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (PartCounter.Parts == 5)
        {
            End.End();
        }
        else
        {
            PartCounter.ShowGui();
        }
    }
}
