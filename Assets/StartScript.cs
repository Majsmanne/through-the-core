﻿using UnityEngine;

public class StartScript : MonoBehaviour
{

    public GameObject Player;
	// Use this for initialization
	void Start ()
	{
	    var move = GetComponent<CutsceneMove>();
	    move.OnFinish = ToControl;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void ToControl()
    {
        Player.SetActive(true);
        gameObject.SetActive(false);
    }
}
